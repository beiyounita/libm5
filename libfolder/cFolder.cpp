#include "cFolder.h"
#include <windows.h>
#include <vector>
#include <fstream>
#include <io.h>

static bool isVer(std::ifstream& ifile, const std::string& filename) {
	// -1 不存在/权限
	if (::_access(filename.c_str(), 0)) {
		return false;
	}
	ifile.open(filename);
	if (!ifile.is_open()) {
		return false;
	}
	std::string line;
	while (getline(ifile, line)) {
		if (line.find("Version") != std::string::npos && line.find("3.0.169") != std::string::npos) {
			ifile.close();
			return true;
		}
	}
	ifile.close();
	return false;
}


static bool isVers(std::ifstream& ifile, const std::string& path) {
	const std::vector<std::string> files = { "res.ini", "update.ini" };
	for (const auto& file : files) {
		if (!isVer(ifile, path + file)) {
			return false;
		}
	}
	return true;
}

static void enumDir(const std::string& path, std::string& path_found) {
	_finddata_t file_data;
	auto str_temp = path + "*";
	auto handle = _findfirst(str_temp.c_str(), &file_data);
	if (handle == -1L) {
		return;
	}
	int subdir = 0;
	int subfile = 0;
	bool res = false;
	bool upd = false;
	do {
		if (file_data.attrib & _A_SUBDIR) {
			if (strcmp(file_data.name, "common") == 0 || strcmp(file_data.name, "scene") == 0) {
				++subdir;
			}
		} else if (strcmp(file_data.name, "res.ini") == 0){
			res = true;
		} else if (strcmp(file_data.name, "update.ini") == 0) {
			upd = true;
		} else {
			++subfile;
		}

	} while (_findnext(handle, &file_data) == 0);
	_findclose(handle);

	if (subdir >= 2 && subfile >= 40 && res && upd) {
		std::ifstream ifile;
		if (isVers(ifile, path)) {
			path_found = path;
			return;
		}
	}

	handle = _findfirst(str_temp.c_str(), &file_data);
	do {
		if (file_data.attrib & _A_SUBDIR) {
			if ((strcmp(file_data.name, ".") != 0) && (strcmp(file_data.name, "..") != 0)) {
				str_temp = path + file_data.name + "/";
				enumDir(str_temp, path_found);
				if (!path_found.empty()) {
					return;
				}
			}
		}
	} while (_findnext(handle, &file_data) == 0);
	_findclose(handle);
}




static bool is169(std::ifstream& ifile, std::string& pathori) {
	const std::vector<std::string> paths = { "", "MHXY-JD-3.0.169/", "梦幻西游/" };
	for (const auto& path : paths) {
		if (isVers(ifile, pathori + path)) {
			pathori += path;
			return true;
		}
	}
	return false;
}


std::string get169(HWND hwnd, std::string media_path) {
	if (media_path.size()) {
		if (media_path.back() == '\\') {
			media_path.pop_back();
		}
		if (media_path.back() != '/') {
			media_path += '/';
		}
	}
	media_path += "client";
	bool rewrite = true;
	std::string path_found;
	if (::_access(media_path.c_str(), 0)) {
	} else {
		std::ifstream ifile;
		ifile.open(media_path);
		if (ifile.is_open()) {
			//  不要 >> 小心路径空格
			getline(ifile, path_found);
			ifile.close();
			if (isVers(ifile, path_found)) {
				rewrite = false;
			}
		}
	}

	if (!rewrite) {
		return path_found;
	}

	path_found.clear();
	std::string path;
	std::ifstream ifile;
	SetWindowText(hwnd, "正在寻找客户端，需要几分钟，请耐心等待...");
	if (true) {
		const std::vector<std::string> paths = {
			"Program Files (x86)/梦幻西游/",
			"Program Files (x86)/",
			"Program Files /梦幻西游/",
			"Program Files /",
			"Program Files/梦幻西游/",
			"Program Files/",
			"梦幻西游/",
			"",
		};

		for (char c = 'C'; c <= 'N'; ++c) {
			for (const auto& p : paths) {
				path = c;
				path = path + ":/" + p;
				if (isVers(ifile, path)) {
					path_found = path;
					break;
				}
			}
			if (path_found.size()) {
				break;
			}
		}
	}

	if (path_found.empty()) {
		char arr[1024];
		memset(arr, 0, sizeof(arr));
		memcpy(arr, __argv[0], strlen(__argv[0]));
		int length = -1;
		while (arr[++length] != 0) {
			if (arr[length] == '\\') {
				arr[length] = '/';
			}
		}
		int pos = 0;
		while (length > 0) {
			if (arr[--length] != '/') {
				continue;
			}
			arr[length + 1] = 0;
			path = arr;
			if (is169(ifile, path)) {
				path_found = path;
				break;
			}
		}
	}

	if (path_found.empty()) {
		for (char c = 'C'; c <= 'N'; ++c) {
			path = c;
			path = path + ":/";
			path_found.clear();
			enumDir(path, path_found);
			if (path_found.size()) {
				break;
			}
		}
	}
	if (path_found.size()) {
		std::ofstream ofile(media_path);
		if (ofile.is_open()) {
			ofile << path_found;
			ofile.close();
		}
	}

	return path_found;
}